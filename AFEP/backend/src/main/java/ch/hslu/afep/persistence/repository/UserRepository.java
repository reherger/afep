package ch.hslu.afep.persistence.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ch.hslu.afep.persistence.model.User;

public interface UserRepository extends CrudRepository<User, Integer>{

  User findByUsername(String username);
  List<User> findAll();
  
}