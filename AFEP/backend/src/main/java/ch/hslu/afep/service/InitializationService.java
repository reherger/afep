package ch.hslu.afep.service;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.hslu.afep.persistence.repository.UserRepository;

@Service
public class InitializationService {

  private static final Logger LOGGER = LoggerFactory.getLogger(InitializationService.class);
  
  @Autowired
  public UserRepository userRepo;
  
  @PostConstruct
  public void init() {
    LOGGER.debug("initializing");
  }
  
  
}
