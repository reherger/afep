package ch.hslu.afep.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.hslu.afep.models.LoginResponse;
import ch.hslu.afep.persistence.model.User;
import ch.hslu.afep.service.security.JwtTokenUtil;
import ch.hslu.afep.service.security.UserCredentialsService;
import ch.hslu.afep.service.security.UserDetailsServiceImpl;

@RestController
@CrossOrigin
@RequestMapping("/api/public")
public class AuthController {

  private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);
  
  @Autowired
  private UserCredentialsService userCredentialsService;
  @Autowired
  private UserDetailsServiceImpl userDetailsService;
  @Autowired
  private AuthenticationManager authenticationManager;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  
  @PostMapping(path = "/register")
  public void addUser(@RequestBody User user) {
    LOGGER.debug("saving user [{}]", user);
    userCredentialsService.signUp(user);
  }
  
  @PostMapping(path = "/login")
  public LoginResponse login(@RequestBody User login) {
    // authentication is done by UserDetailsService automatically
    LOGGER.debug("Got login for username [{}]", login.getUsername());
    try {
      authenticate(login.getUsername(), login.getPassword());
      UserDetails userDetails = userDetailsService.loadUserByUsername(login.getUsername());
      return jwtTokenUtil.generateToken(userDetails);
    } catch (BadCredentialsException e) {
      LOGGER.error(e.getMessage());
      return null;
    }
  }
  
  private void authenticate(String username, String password)  {
    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
  }
  
}
