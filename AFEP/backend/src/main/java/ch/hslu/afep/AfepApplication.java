package ch.hslu.afep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AfepApplication {

	public static void main(String[] args) {
		SpringApplication.run(AfepApplication.class, args);
	}
}
