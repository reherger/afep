package ch.hslu.afep.service.security;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ch.hslu.afep.persistence.model.User;
import ch.hslu.afep.persistence.repository.UserRepository;


/**
 * Spring User details service implementation for authentication.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  
  private static final String ROLE = "CollabCoin-User";

  @Autowired
  private UserRepository userRepo;
  
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepo.findByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException("User not found");
    }
    List<SimpleGrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority(ROLE));
    return new org.springframework.security.core.userdetails.User(username, user.getPassword(), authorities);
  }

}
