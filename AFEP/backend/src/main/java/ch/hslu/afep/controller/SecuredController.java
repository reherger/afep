package ch.hslu.afep.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.hslu.afep.persistence.model.User;
import ch.hslu.afep.persistence.repository.UserRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/secured")
public class SecuredController {

  private static final Logger LOGGER = LoggerFactory.getLogger(SecuredController.class);
  
  @Autowired
  public UserRepository userRepo;
  
  @GetMapping(path = "/users")
  public List<User> getAllUsers() {
    LOGGER.debug("getting all users");
    List<User> users = userRepo.findAll();
    // remove password and seed
    for (User u : users) {
      u.setPassword(null);
      u.setSeed(null);
    }
    return users;
  }
}
