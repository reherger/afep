package ch.hslu.afep.service.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import ch.hslu.afep.models.LoginResponse;
import ch.hslu.afep.persistence.model.User;
import ch.hslu.afep.persistence.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil {

  public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

  @Autowired
  private UserRepository userRepo;
  
  @Value("${jwt.secret}")
  private String secret;

  public String getUsernameFromToken(String token) {
    return getClaimFromToken(token, Claims::getSubject);
  }

  public Date getExpirationDateFromToken(String token) {
    return getClaimFromToken(token, Claims::getExpiration);
  }

  public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
    final Claims claims = getAllClaimsFromToken(token);
    return claimsResolver.apply(claims);
  }

  private Claims getAllClaimsFromToken(String token) {
    return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
  }

  private Boolean isTokenExpired(String token) {
    final Date expiration = getExpirationDateFromToken(token);
    return expiration.before(new Date());
  }

  public LoginResponse generateToken(UserDetails userDetails) {
    Map<String, Object> claims = new HashMap<>();
    claims.put("issuer", "CollabCoin");
    List<String> roles = new ArrayList<>();
    for (GrantedAuthority auth: userDetails.getAuthorities()) {
      roles.add(auth.getAuthority());
    }
    claims.put("roles", roles.toArray());
    User user = userRepo.findByUsername(userDetails.getUsername());
    user.setPassword(null);
    user.setSeed(null);
    LoginResponse login = new LoginResponse();
    login.setJwt(doGenerateToken(claims, userDetails.getUsername()));
    login.setUser(user);
    return login;
  }

  private String doGenerateToken(Map<String, Object> claims, String subject) {
    return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
        .signWith(SignatureAlgorithm.HS512, secret).compact();
  }

  public Boolean validateToken(String token, UserDetails userDetails) {
    final String username = getUsernameFromToken(token);
    return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
  }
}
