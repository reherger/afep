package ch.hslu.afep.service.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import ch.hslu.afep.persistence.model.User;
import ch.hslu.afep.persistence.repository.UserRepository;


@Service
public class UserCredentialsService {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserCredentialsService.class);

  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  private UserRepository userRepo;

  public void signUp(User userToSignUp) {
    userToSignUp.setPassword(passwordEncoder.encode(userToSignUp.getPassword()));
    try {
      userRepo.save(userToSignUp);
    } catch (Exception e) {
      LOGGER.error(e.getMessage());
    }
  }
}
