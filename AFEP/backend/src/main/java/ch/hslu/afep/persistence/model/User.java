package ch.hslu.afep.persistence.model;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = { "username" }))
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;
  private String username;
  private String password;
  private String wavesAddress;
  @Basic(fetch = FetchType.LAZY)
  private String seed; // is saved for recovering of account, should be kicked when productive
  private String firstname;
  private String lastname;
  private String email;
  private String company;
  private String description;
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getUsername() {
    return username;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public String getWavesAddress() {
    return wavesAddress;
  }
  public void setWavesAddress(String wavesAddress) {
    this.wavesAddress = wavesAddress;
  }
  public String getSeed() {
    return seed;
  }
  public void setSeed(String seed) {
    this.seed = seed;
  }
  public String getFirstname() {
    return firstname;
  }
  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }
  public String getLastname() {
    return lastname;
  }
  public void setLastname(String lastname) {
    this.lastname = lastname;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getCompany() {
    return company;
  }
  public void setCompany(String company) {
    this.company = company;
  }
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("User [id=");
    builder.append(id);
    builder.append(", username=");
    builder.append(username);
    builder.append(", password=");
    builder.append(password);
    builder.append(", wavesAddress=");
    builder.append(wavesAddress);
    builder.append(", seed=");
    builder.append(seed);
    builder.append(", firstname=");
    builder.append(firstname);
    builder.append(", lastname=");
    builder.append(lastname);
    builder.append(", email=");
    builder.append(email);
    builder.append(", company=");
    builder.append(company);
    builder.append(", description=");
    builder.append(description);
    builder.append("]");
    return builder.toString();
  }
}
