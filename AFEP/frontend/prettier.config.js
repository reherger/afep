module.exports = {
  arrowParens: 'always',
  bracketSpacing: false,
  printWidth: 120,
  semi: false,
  singleQuote: true,
  trailingComma: 'none'
}
