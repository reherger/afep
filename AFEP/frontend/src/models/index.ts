export interface User {
  id?: number
  username: string
  password?: string
  wavesAddress?: string
  seed?: string
  firstname?: string
  lastname?: string
  email?: string
  company?: string
  description?: string
  rewards?: Reward[]
}

export interface RankingEntry {
  type?: RewardType
  userId: number
  amount: number
  rank?: number
}

export interface Reward {
  type: RewardType
  assetId: string
  amount: number
}

export enum RewardType {
  BEING_NICE = 'Being nice',
  MATERIAL = 'Material',
  REPRESENTING = 'Representing',
  CONNECTING = 'Connecting',
  CLEANING = 'Cleaning',
  CHEERING = 'Cheering',
  GREAT_IDEA = 'Great Idea',
  CONVERSATION = 'Conversation',
  WHATEVER = 'Whatever'
}

export interface RewardMapping {
  type: RewardType
  assetId: string
}

export const assetIdMapping: RewardMapping[] = [
  {type: RewardType.BEING_NICE, assetId: '5rvNfrGcd9YfFcs7T7wFY9ArArA593RJxXYT8bxJcUcV'},
  {type: RewardType.MATERIAL, assetId: '4RVMtSxRV2popyupZU4agkK2ozjwz9iDT7HTsr31Njjm'},
  {type: RewardType.REPRESENTING, assetId: 'Bo561ehXvKrYqauCKQAZLQQXuLRSyUgHU79TkD4Rm3fs'},
  {type: RewardType.CONNECTING, assetId: 'GgUMz4wdHZmzecwjxfT54yuZgDNXjvy8YuaA1zoZKGCH'},
  {type: RewardType.CLEANING, assetId: 'GwL85aAUuCafvJk8u85JDtSanbN12s6rDQKk9vzaSFdu'},
  {type: RewardType.CHEERING, assetId: 'FjYEKGhiSgsMnk4eYD6hGPCwxN9g7QHCeBp3seXUFs3h'},
  {type: RewardType.GREAT_IDEA, assetId: 'HiK53evTgZcYaaJrc8oM9hGGWJxYqc4zLUPGvfy9CWs2'},
  {type: RewardType.CONVERSATION, assetId: '97YRsn4mNAGi6xhF86XY88adJwu8HrcCX1J2QEzAGcBU'},
  {type: RewardType.WHATEVER, assetId: 'CCaMbPL1ryn2UpHmYZyUj7WAYk4nwT3bo2koRXfr7Km7'}
]

export interface AssetIdDistributionResult {
  [wavesAddress: string]: string
}

export interface Contact {
  name: string
  company: string
  did: string
}
