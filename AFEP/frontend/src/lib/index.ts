import {
  transfer,
  data,
  broadcast,
  ITransferTransaction,
  IDataTransaction,
  WithId,
  seedUtils
} from '@waves/waves-transactions'
import {RewardType} from '@/models'
import {assetIdMapping} from '@/models'
import {find, propEq} from 'ramda'

// seed of account that created and owns all the tokens
const rootSeedPhrase = 'dilemma glow feature fee custom asset myth visa custom appear iron gadget tree shuffle purse'
export const rootAddress = '3N9jk8TREvENXGtGiurX87c328yEPKbM8QV'
const nodeUrl = 'https://testnodes.wavesnodes.com' // testnet - mainnet: https://nodes.wavesplatform.com
// testnet according to https://stackoverflow.com/questions/54922988/how-to-use-waves-transactions-library-in-testnet
const chainId = 'T'

export const transferToken = async (address: string, assetId: string) => {
  const params = {
    amount: 1,
    recipient: address, // the address to recieve the token
    assetId,
    fee: '100000' // 100000 Waves for transaction (0.001)
  }
  const signedTransferTx: ITransferTransaction & WithId = transfer(params, rootSeedPhrase)
  const response = await broadcast(signedTransferTx, nodeUrl)
  console.log(response)
}

export const transferWaves = async (recipient: string) => {
  const params = {
    amount: 100000, // 100000 to have enough to set name to account storage (should cost 0.001 for 1KB)
    recipient,
    fee: '100000'
  }
  const signedTransferTx: ITransferTransaction & WithId = transfer(params, rootSeedPhrase)
  const response = await broadcast(signedTransferTx, nodeUrl)
}

/**
 * Sets the name key in the users account data storage.
 * @param name the users name to add to the account data storage
 * @param seedPhrase the seed phrase of the users account
 */
export const dataTransaction = async (name: string, company: string, seedPhrase: string) => {
  const dataEntry = [
    {
      key: 'name',
      type: 'string',
      value: name
    },
    {
      key: 'company',
      type: 'string',
      value: company
    }
  ]
  const params = {
    data: dataEntry
  }

  const dataTx: IDataTransaction & WithId = data(params, seedPhrase)
  const response = await broadcast(dataTx, nodeUrl)
  return response
}

export const createSeed = async () => {
  const seedPhrase: string = seedUtils.generateNewSeed()
  const newSeed = new seedUtils.Seed(seedPhrase, chainId)
  return newSeed
}

const timeout = (ms: number) => {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

export interface NewAccountResponse {
  seed: seedUtils.Seed
}

export const createNewWavesAccount = async (): Promise<NewAccountResponse> => {
  // create new seed
  const newSeed = await createSeed()
  await timeout(500)
  return {seed: newSeed} as NewAccountResponse
}

export const getRewardTypeFromAssetId = (assetId: string) => {
  const mapping = find(propEq('assetId', assetId), assetIdMapping)
  if (mapping) {
    return mapping.type
  }
}

export const getAssetIdFromRewardType = (rewardType: RewardType) => {
  const mapping = find(propEq('type', rewardType), assetIdMapping)
  if (mapping) {
    return mapping.assetId
  }
}
