import {RewardType} from '@/models'

export const getIconFromRewardType = (type: RewardType) => {
  switch (type) {
    case RewardType.BEING_NICE:
      return 'el-icon-sunny'
    case RewardType.MATERIAL:
      return 'el-icon-s-open'
    case RewardType.REPRESENTING:
      return 'el-icon-s-custom'
    case RewardType.CONNECTING:
      return 'el-icon-connection'
    case RewardType.CLEANING:
      return 'el-icon-sunny'
    case RewardType.CHEERING:
      return 'el-icon-cold-drink'
    case RewardType.GREAT_IDEA:
      return 'el-icon-s-opportunity'
    case RewardType.CONVERSATION:
      return 'el-icon-chat-square'
    case RewardType.WHATEVER:
      return 'el-icon-info'

    default:
      return 'el-icon-info'
  }
}
