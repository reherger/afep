import Vue from 'vue'
import Vuex from 'vuex'
import {IUserState} from './modules/userModule'
import {IMessageState} from './modules/messages'
import {IAllUsersState} from './modules/allUsersModule'

Vue.use(Vuex)

export interface IRootState {
  user: IUserState
  users: IAllUsersState
  messages: IMessageState
}

// Declare empty store first, dynamically register all modules later.
export default new Vuex.Store<IRootState>({})
