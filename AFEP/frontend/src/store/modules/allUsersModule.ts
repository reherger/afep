import {VuexModule, Module, Mutation, MutationAction, getModule} from 'vuex-module-decorators'
import store from '..'
import {User, assetIdMapping, RewardMapping, RewardType, RankingEntry} from '@/models'
import {find, propEq} from 'ramda'
import {getAssetDistribution, AssetDistributionResponse} from '@/api/waves'
import {getRewardTypeFromAssetId} from '@/lib'
import {getAllUsers} from '@/api'

export interface IAllUsersState {
  users: User[]
  ranking: Map<RewardType, RankingEntry[]>
}

@Module({dynamic: true, store, name: 'allUsers'})
class AllUserModule extends VuexModule implements IAllUsersState {
  public users: User[] = []
  public ranking: Map<RewardType, RankingEntry[]> = new Map<RewardType, RankingEntry[]>()

  @MutationAction({mutate: ['users']})
  async loadAllUsers() {
    const users: User[] = await getAllUsers() // reload all users
    // get all assets for every user
    for (const mapping of assetIdMapping) {
      // distribution for assetId
      const distribution: AssetDistributionResponse[] = await getAssetDistribution(mapping.assetId)
      // put amount of asset id to the users rewards by mapping by user's address
      for (const distr of distribution) {
        const user = find(propEq('wavesAddress', distr.address), users)
        if (user) {
          if (!user.rewards) {
            user.rewards = [] // init if no rewards array present
          }
          user.rewards.push({
            type: getRewardTypeFromAssetId(mapping.assetId) || RewardType.WHATEVER,
            assetId: mapping.assetId,
            amount: distr.amount
          })
        }
      }
    }
    return {users}
  }

  @Mutation
  async prepareRanking() {
    // ranking
    const ranking = new Map<RewardType, RankingEntry[]>()
    for (const mapping of assetIdMapping) {
      const entries: RankingEntry[] = []
      const type: RewardType = mapping.type
      for (const user of this.users) {
        if (user.rewards) {
          // get the reward entry with RewardType type
          const userRewardEntry = find(propEq('type', type), user.rewards)
          if (userRewardEntry) {
            // rank will be set later
            const entry: RankingEntry = {userId: user.id as number, amount: userRewardEntry.amount}
            entries.push(entry)
          }
        }
      }
      // sort entries by amount
      entries.sort((a, b) => {
        const amountA = a.amount
        const amountB = b.amount
        if (amountA > amountB) {
          return -1
        }
        if (amountA < amountB) {
          return 1
        }
        return 0
      })
      // set the ranks
      // users with same amount should have the same rank
      let i = 0
      let lastAmount: number
      entries.forEach((entry) => {
        if (!lastAmount || lastAmount !== entry.amount) {
          i++ // only increment rank if last amount is not the same as the current
        }
        entry.rank = i
        lastAmount = entry.amount
      })
      if (entries.length > 0) {
        ranking.set(type, entries)
      }
    }
    this.ranking = ranking
  }

  @Mutation
  setUsers(users: User[]) {
    this.users = users
  }

  get allUsers() {
    return this.users
  }

  get rankingForUserId() {
    return (userId: number): RankingEntry[] => {
      const userRanking: RankingEntry[] = []
      for (const [type, entries] of this.ranking) {
        const rankingEntry = find(propEq('userId', userId), entries)
        if (rankingEntry) {
          userRanking.push({type, userId, amount: rankingEntry.amount, rank: rankingEntry.rank})
        }
      }
      return userRanking
    }
  }
}

export const AllUsersModule = getModule(AllUserModule)
