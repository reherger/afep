import {VuexModule, Module, Mutation, getModule} from 'vuex-module-decorators'
import store from '..'
import {User, Reward} from '@/models'

export interface IUserState {
  user: Partial<User>
  jwt: string
  isloggedIn: boolean
}

@Module({dynamic: true, store, name: 'user'})
class UsersModule extends VuexModule implements IUserState {
  public user: Partial<User> = {}
  public jwt: string = ''
  public isloggedIn = false

  @Mutation
  setUserRewards(rewards: Reward[]) {
    this.user.rewards = rewards
  }

  @Mutation
  logout() {
    this.user = {}
    this.isloggedIn = false
  }

  @Mutation
  login(loginResponse: {jwt: string; user: User}) {
    this.user = loginResponse.user
    this.jwt = loginResponse.jwt
    this.isloggedIn = true
  }
}

export const UserModule = getModule(UsersModule)
