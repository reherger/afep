import {VuexModule, Module, Mutation, Action, getModule} from 'vuex-module-decorators'
import store from '..'

export interface IMessageState {
  errors: string[]
  infos: string[]
}

@Module({dynamic: true, store, name: 'messages'})
class MessageModule extends VuexModule implements IMessageState {
  public errors: string[] = []
  public infos: string[] = []

  @Mutation
  addError(error: string) {
    this.errors.push(error)
  }

  @Mutation
  removeError(index: number) {
    this.errors.splice(index, 1)
  }

  @Mutation
  clearAllErrors() {
    this.errors = []
  }

  get hasErrors() {
    return this.errors.length > 0
  }

  @Mutation
  addInfo(info: string) {
    this.infos.push(info)
  }

  @Mutation
  removeInfo(index: number) {
    this.infos.splice(index, 1)
  }

  @Mutation
  clearAllInfos() {
    this.infos = []
  }

  get hasInfos() {
    return this.infos.length > 0
  }

  @Mutation
  clear() {
    this.errors = []
    this.infos = []
  }
}

export const MessagesModule = getModule(MessageModule)
