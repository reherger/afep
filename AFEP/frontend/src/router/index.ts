import Vue from 'vue'
import Router from 'vue-router'
import {MessagesModule} from '@/store/modules/messages'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      component: () => import('@/views/Home.vue'),
      meta: {
        title: 'CollabCoin'
      }
    },
    {
      path: '/reward',
      component: () => import('@/views/Reward.vue'),
      meta: {
        title: 'Reward'
      }
    },
    {
      path: '/compare',
      component: () => import('@/views/Compare.vue'),
      meta: {
        title: 'Compare'
      }
    },
    {
      path: '/tests',
      component: () => import('@/views/Tests.vue'),
      meta: {
        title: 'Tests'
      }
    },
    {
      path: '/register',
      component: () => import('@/views/Register.vue'),
      meta: {
        title: 'Register'
      }
    }
  ]
})

router.beforeEach((to, from, next: () => void) => {
  MessagesModule.clear()
  next()
})

router.afterEach((to, from) => {
  document.title = to.meta.title
})

export default router
