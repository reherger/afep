import axios from 'axios'
import {AllUsersModule} from '@/store/modules/allUsersModule'

const config = {
  baseURL: 'https://testnodes.wavesnodes.com/'
}

export interface AccountDataResponse {
  type: string
  value: string
  key: 'name' | 'company'
}

export const getUserDataByAddress = async (address: string) => {
  try {
    const response = await axios.get(`/addresses/data/${address}`, config)
    const data: AccountDataResponse[] = response.data
    return data
  } catch (error) {
    console.error(error)
    return []
  }
}

export interface AssetDistributionResponse {
  address: string
  amount: number
}

export const getAssetDistribution = async (assetId: string) => {
  try {
    const response = await axios.get(`/assets/${assetId}/distribution`, config)
    const data = response.data
    const result: AssetDistributionResponse[] = []
    Object.entries(data).forEach((distr) => result.push({address: distr[0], amount: distr[1] as number}))
    return result
  } catch (error) {
    console.error(error)
    return []
  }
}
