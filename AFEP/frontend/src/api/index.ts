import axios from 'axios'
import {AllUsersModule} from '@/store/modules/allUsersModule'
import {UserModule} from '@/store/modules/userModule'
import {MessagesModule} from '@/store/modules/messages'
import router from '@/router'
import {User} from '@/models'

const config = {
  baseURL:
    process.env.VUE_APP_AXIOS_BASE_PATH !== undefined
      ? process.env.VUE_APP_AXIOS_BASE_PATH
      : 'http://localhost:5000/api'
}
export const axiosHeaders = () => {
  return {Authorization: 'Bearer ' + UserModule.jwt}
}

const API_ERROR = 'Es ist ein Fehler aufgetreten'

export const getAllUsers = async (): Promise<User[]> => {
  try {
    const response = await axios.get('/secured/users', {...config, ...{headers: axiosHeaders()}})
    if (response && response.status === 200 && response.data) {
      return response.data
    } else {
      MessagesModule.addError(API_ERROR)
      return []
    }
  } catch (error) {
    console.error(error)
    MessagesModule.addError(API_ERROR)
    return []
  }
}

export const login = async (user: User) => {
  try {
    const response = await axios.post('/public/login', user, config)
    if (response && response.status === 200 && response.data) {
      UserModule.login(response.data)
      router.push('/')
    } else {
      MessagesModule.addError(API_ERROR)
    }
  } catch (error) {
    console.error(error)
    MessagesModule.addError(API_ERROR)
  }
}

export const register = async (user: User) => {
  try {
    const response = await axios.post('/public/register', user, config)
    if (response && response.status === 200) {
      router.push('/')
      MessagesModule.addInfo('Sie haben sich erfolgreich registriert - Sie können sich jetzt anmelden')
    } else {
      MessagesModule.addError(API_ERROR)
    }
  } catch (error) {
    console.error(error)
    MessagesModule.addError(API_ERROR)
  }
}
