import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import elementLocale from 'element-ui/lib/locale'
import elementLangDe from 'element-ui/lib/locale/lang/de'
import 'element-ui/lib/theme-chalk/display.css'
import './assets/styles.css'
import 'nprogress/nprogress.css'

Vue.config.productionTip = false

elementLocale.use(elementLangDe)
Vue.use(ElementUI)

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
